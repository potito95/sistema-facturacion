-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3308
-- Tiempo de generación: 26-04-2022 a las 19:54:20
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `doombook`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `autor`
--

CREATE TABLE `autor` (
  `Dni_autor` mediumint(9) NOT NULL,
  `Nombre_autor` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `autor`
--

INSERT INTO `autor` (`Dni_autor`, `Nombre_autor`) VALUES
(1, 'G RR Martin'),
(2, 'Akira Toriyama'),
(3, 'Ken Waiku'),
(4, 'Hiroyuki Takei'),
(5, 'Yusei Matsue'),
(6, 'Tite Kubo'),
(7, 'Ross Molico'),
(8, 'Yuka Kitagawa'),
(9, 'Jeong Gyeong Yu'),
(10, 'Nozomi Mino'),
(11, 'Aya Kanno'),
(12, 'Khota Hirano'),
(13, 'Arata Miyasuki'),
(14, 'Yoshimura Kana'),
(15, 'Tsukasa Hojos'),
(16, 'Tasuku Karasuma'),
(17, 'Boichi'),
(18, 'Miguel de Cerva'),
(19, 'Leopoldo Alas C'),
(20, 'Fernando de Roj'),
(21, 'Diego Hurtado d'),
(22, 'Emily Brontë'),
(23, 'Aldous Huxley'),
(24, 'William Golding'),
(25, 'Federio García '),
(26, 'Oscar Wilde'),
(27, 'Bram Stoker'),
(28, 'Stephen King'),
(29, 'Patrick Süskind'),
(30, 'Max Brooks'),
(31, 'Mary Shelley'),
(32, 'William Peter B'),
(33, 'Bret Easton Ell'),
(34, 'Ira Levin'),
(35, 'Mark Twain'),
(36, 'Robert Louis St'),
(37, 'Daniel Defoe'),
(38, 'Herman Melville'),
(39, 'Julio Verne'),
(40, 'Rudyard Kipling'),
(41, 'Jonathan Swift');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `editorial`
--

CREATE TABLE `editorial` (
  `Cod_editorial` mediumint(9) NOT NULL,
  `Nombre_editorial` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `editorial`
--

INSERT INTO `editorial` (`Cod_editorial`, `Nombre_editorial`) VALUES
(1, 'Planeta'),
(2, 'Alfaguara'),
(3, 'Ediciones SM'),
(4, 'Anaya'),
(5, 'Ediciones Vicen'),
(6, 'Alianza'),
(7, 'Debolsillo'),
(8, 'Catedra'),
(9, 'Austral'),
(10, 'Alma'),
(11, 'Booket'),
(12, 'Círculo de lect'),
(13, 'Picador'),
(14, 'RBA'),
(15, 'Norma'),
(16, 'Ivrea'),
(17, 'Panini'),
(18, 'Kitsune Manga'),
(19, 'Luis Vives');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido`
--

CREATE TABLE `pedido` (
  `Cod_pedido` mediumint(9) NOT NULL,
  `Cod_usuario` mediumint(9) NOT NULL,
  `Valor_total_pedido` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_publicacion`
--

CREATE TABLE `pedido_publicacion` (
  `Cod_pedido` mediumint(9) NOT NULL,
  `Cod_interno` mediumint(9) NOT NULL,
  `Fecha_hora` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicacion`
--

CREATE TABLE `publicacion` (
  `Cod_interno` mediumint(9) NOT NULL,
  `Cod_publico` bigint(15) NOT NULL,
  `Titulo` varchar(40) NOT NULL,
  `Descripcion_publicacion` text NOT NULL,
  `Cod_editorial` mediumint(9) NOT NULL,
  `Stock_publicacion` int(3) NOT NULL,
  `Precio_publicacion` float NOT NULL,
  `img` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `publicacion`
--

INSERT INTO `publicacion` (`Cod_interno`, `Cod_publico`, `Titulo`, `Descripcion_publicacion`, `Cod_editorial`, `Stock_publicacion`, `Precio_publicacion`, `img`) VALUES
(19, 8420412147, 'Don Quijote de la Mancha', 'Esta obra narra las aventuras de Alonso Quijano, un hidalgo pobre que de tanto leer novelas de caballería acaba enloqueciendo y creyendo ser un caballero andante.', 2, 7, 15, 'quijoterr.jpg'),
(21, 8467585978, 'La Regenta', 'La ciudad de Vetusta, símbolo de tradiciones anacrónicas y opresivas, es el reino de una hipocresía y de una intolerancia que hacen irrespirable y trágica la vida de Ana Ozores.', 3, 4, 10, 'regentar.jpg'),
(23, 8466751704, 'La Celestina', 'En La Celestina se muestran los trágicos amores de Calisto y Melibea y las malas artes que emplea la alcahueta Celestina para que se enamoren.', 4, 17, 12, 'celestinar.jpg'),
(25, 8431680253, 'El Lazarillo de Tormes', 'La novela gira entorno a Lázaro, un niño ingenuo que, debido a las adversidades y complejidades que atraviesa, se convierte en un joven pícaro que lucha por sobrevivir.', 5, 2, 12, 'lazarillo.jpg'),
(26, 8491819436, 'Cumbres Borrascosas', 'Situada en los sombríos y desolados páramos de Yorkshire, constituye una asombrosa visión metafísica del destino, la obsesión, la pasión y la venganza.', 6, 25, 23, 'cumbres.jpg'),
(27, 8497594258, 'Un Mundo Feliz', 'La novela describe un mundo en el que triunfan los dioses del consumo y la comodidad.', 7, 9, 5.37, 'mundo.jpg'),
(28, 8420674176, 'El Señor de las Moscas', 'El Señor de las Moscas cuenta la historia de un grupo de chicos británicos cuyo avión ha chocado en una isla desierta en el Océano Pacífico.', 6, 6, 17, 'moscas.jpg'),
(29, 8437622453, 'La Casa de Bernarda Alba', 'Esta obra se centra en la tiranía moral y la represión sexual que Bernarda ejerce sobre sus hijas durante una imposición de aislamiento que dura 8 años.', 8, 2, 7.5, 'bernarda.jpg'),
(30, 8467033932, 'El Retrato de Dorian Gray', 'El joven Dorian desea permanecer por siempre joven y bello, y que, a cambio, su retrato sea el que envejezca. Seducido por un aristócrata cínico, Dorian disfruta del placer y los excesos, y lleva a otras personas a la perdición.', 9, 7, 12, 'dorian.jpg'),
(31, 8415618832, 'Drácula', 'El abogado Jonathan Harker descubre que, en el castillo del conde Drácula, este se comporta por las noches como un vampiro.', 10, 14, 17.42, 'dracula.jpg'),
(32, 8497595696, 'Carrie', 'Acerca de una introvertida adolescente, Carrie White, quien descubre que posee poderes telequinéticos que salen a la luz en el momento que ella estalla de ira.', 7, 3, 20, 'carrie.jpg'),
(33, 8432251143, 'El Perfume', 'Jean Baptiste vive obsesionado con la idea de atrapar los olores de algunas mujeres.', 11, 4, 15.5, 'perfume.jpg'),
(34, 8466345682, 'Misery', 'Un escritor sufre un grave accidente y recobra el conocimiento en una apartada casa en la que vive una misteriosa mujer, corpulenta y de extraño carácter.', 7, 32, 7.99, 'misery.jpg'),
(35, 8490624526, 'Guerra Mundial Z', 'La novela es un conjunto de entrevistas individuales narradas por un agente de la Comisión de Posguerra de las Naciones Unidas con posterioridad a un devastador conflicto global contra la plaga zombi.', 7, 8, 25, 'guerra.jpg'),
(36, 8467039493, 'Frankenstein', 'Víctor Frankenstein le da vida a un ser creado a partir de diferentes partes de cadáveres.', 9, 3, 15, 'frank.jpg'),
(37, 8422606615, 'El Exorcista', 'Se basa en un exorcismo efectuado en 1949, y del que Blatty oyó hablar en 1950, cuando recibía clases en la Universidad de Georgetown, un centro dirigido por sacerdotes jesuitas.', 12, 15, 17, 'exorcista.jpg'),
(38, 1447277708, 'American Psycho', 'Describe los episodios en la vida de un yuppie asesino de Manhattan.', 13, 0, 23, 'psycho.jpg'),
(39, 8447301990, 'La Semilla del Diablo', 'Un joven matrimonio se instala en un céntrico apartamento de Nueva York sin sospechar que sus ancianos vecinos pertenecen a una poderosa secta satánica que espera la llegada de su nuevo mesías.', 14, 5, 10, 'semilla.jpg'),
(40, 8420487066, 'Las Aventuras de Tom Sawyer', 'Relata las aventuras de la infancia de Tom Sawyer, un niño que crece en el período anterior a la Guerra de Secesión.', 2, 13, 12, 'tom.jpg'),
(41, 8466794999, 'La Isla del Tesoro', 'En esta novela, un mapa de una isla cae en manos del joven Jim Hawkins. El mapa indica el lugar donde el mayor pirata de todos los tiempos ha escondido su botín.', 4, 7, 14.4, 'tesoro.jpg'),
(42, 8431668040, 'Robinson Crusoe', 'Narra las aventuras de un marino que naufraga en una isla solitaria donde debe encontrar la forma de procurar su supervivencia.', 5, 23, 10.26, 'robinson.jpg'),
(43, 8408137220, 'Moby-Dick', 'Narra la travesía del barco ballenero Pequod en la obsesiva y autodestructiva persecución de un gran cachalote blanco.', 9, 50, 17.82, 'moby.jpg'),
(44, 8469836099, 'La Vuelta al Mundo en Ochenta Días', 'Phileas Fogg se compromete a dar la vuelta al mundo en ochenta días utilizando los medios disponibles en la época para cumplir una apuesta.', 4, 7, 21, 'vuelta.jpg'),
(45, 8467860952, 'Viaje al centro de la Tierra', 'Trata de la expedición de un profesor de mineralogía, su sobrino y un guía llamado Hans al interior de la Tierra.', 4, 5, 13.99, 'viaje.jpg'),
(46, 8414011306, 'El Libro de la Selva', 'Después de ser abandonado en la selva, una familia de lobos cría al pequeño cachorro humano Mowgli.', 19, 3, 11.5, 'selva.jpg'),
(47, 8469836072, 'Los Viajes de Gulliver', 'Gulliver es un apasionado de los viajes y sus aventuras lo llevan a lugares extraordinarios.', 4, 18, 13, 'gulliver.jpg'),
(48, 8413623022, 'La Flecha Negra', 'El rico y corrupto Sir Daniel Brackley se enfurece al descubrir que el bandido Flecha Negra le ha robado. Decide entonces enviar a su joven sobrino Richard Shelton a enfrentarse con él.', 6, 0, 4.99, 'flecha.jpg'),
(49, 9788491745, 'DRAGON BALL SUPER Nº74', 'Gracias a la separación forzosa del aura aplicada al revés, Goku consigue el aura de sus compañeros y, con ella, ¡derrota definitivamente a Moro! ', 1, 7, 17.5, 'dragon.jpg'),
(50, 8467947106, 'TOKYO REVENGERS Nº04', ': DA COMIENZO LA BATALLA ENTRE LA TOMAN Y LA WALHALLA: ¡EL HALLOWEEN SANGRIENTO!.La batalla del Halloween sangriento empieza con dos enfrentamientos violentos y repentinos entre los pesos pesados de ambas bandas.', 15, 3, 15.2, 'tokyo.jpg'),
(51, 8419185181, 'SHAMAN KING Nº09', 'Los shamanes son personas que pueden interactuar con los dioses, los espíritus y las almas de los difuntos. Yoh tiene ese poder y quiere convertirse en el rey shaman, alguien capaz de establecer contacto con los Grandes Espíritus.', 16, 5, 13.3, 'shaman.jpg'),
(52, 8467947113, 'TOKYO REVENGERS Nº05', 'EN EL NUEVO PRESENTE DE TAKEMICHI, SE HA CONVERTIDO EN UN PEZ GORDO DE LA TOMAN.La victoria de la Toman en su enfrentamiento contra la Walhalla lleva a la fusión de ambas bandas.', 15, 13, 15.2, 'tokyo-revengers-05.JPG'),
(53, 8467949728, 'ASSASSINATION CLASSROOM Nº01', 'Sr. y Sra. Ivrea se enorgullecen de presentar el manga de espías más desternillante de la historia del espionaje: ¡Spy x Family está en camino!.Los países de Westalis y Ostania libran desde hace años una guerra fría.', 17, 0, 14.5, 'assassination.jpg'),
(54, 8491746430, 'DRAGON BALL SUPER TOMO 15', '¡Siguen las aventuras sin fin de Goku!.Goku consigue acorralar a Moro gracias a haber completado el Ultrainstinto.', 1, 7, 8.55, '1507-1.jpg'),
(55, 8411013116, 'BLEACH MAXIMUM VOLUMEN 21', 'La serie de culto en edición de lujo! Ichigo puede ver espíritus y tiene contacto con el más allá, al que sacará provecho tras conocer a un shinigami que le proporciona la espada a juego con sus habilidades.', 17, 12, 14.25, 'bleach_21.jpg'),
(56, 8418524097, 'FLORECE, FLORECE! Nº02', 'Chita Tobishima se matriculó en una prestigiosa academia de manga pensando que viviría una experiencia llena de emociones y diversión.', 18, 21, 8.55, 'poridentidad.jpg'),
(57, 8467948318, 'Kageno 01', 'TODO PARECÍA IR SOBRE RUEDAS PARA LA RELACIÓN ENTRE KAGENO Y MITSUNAGA, PERO...', 15, 14, 9.55, 'b4ae3dbece522ccc779511c0ff8082aeb09b02c3.jpeg'),
(58, 8491748670, 'MORE THAN WORDS Nº02', 'Mieko (símbolo mujer) cuida cariñosamente de Makki (símbolo hombre) y Eiji (símbolo hombre), que ahora son pareja.', 1, 47, 8.55, '9788491748670.jpg'),
(59, 8418524264, 'QUÉ LE PASA A LA SECRETARIA KIM? Nº02', 'El fenómeno coreano que ha enamorado a los lectores de todo el mundo. Cuando Kim Miso, la secretaria personal de Lee Yeongjun.', 18, 5, 14.5, '1507-1 (1).jpg'),
(60, 8841909611, 'DANGEROUS LOVER Nº01', 'Yuri es una chica guapa y decidida que un día ve algo que no debía en una fiesta. Cuando la están a punto de forzar, Toshiomi Ouya, el líder yakuza del grupo.', 16, 3, 7.5, 'poridentidad (1).jpg'),
(61, 5274804224, 'REQUIEM POR EL REY DE LA ROSA VOL.15', 'A finales de la Edad Media, las casas de York y de Lancaster se enfrentaron por el trono de Inglaterra en una sangrienta sucesión de contiendas, más tarde conocida como la Guerra de las Rosas.', 18, 98, 10.5, '9788418739118.jpg'),
(62, 8846794830, 'KAGENO 10', '¡EL AMOR DE KAGENO POR MITSUNAGA ALCANZA COTAS INSOSPECHADAS!.El último verano de la época de instituto ha llegado y Kageno lo está aprovechando al máximo', 15, 18, 8.55, 'poridentidad (2).jpg'),
(63, 8467942293, 'HELLSING Nº05', 'ÚLTIMO VOLUMEN DE ESTE MANGA DE TERROR GORE DE KOHTA HIRANO.', 15, 27, 15.2, 'poridentidad (3).jpg'),
(64, 8467942293, 'HELLSING Nº05', 'ÚLTIMO VOLUMEN DE ESTE MANGA DE TERROR GORE DE KOHTA HIRANO.', 15, 27, 15.2, 'poridentidad (3).jpg'),
(65, 8846794644, 'CRIMEN PERFECTO Nº10', '¿CONSEGUIRÁ NANJÔ DESTRUIR A USOBUKI?.Tadashi Usobuki es un asesino que manipula los deseos de sus víctimas valiéndose de la sugestión,', 15, 13, 9.5, '579e5c366942c05b3124a9938738f41c2e6b383d.jpeg'),
(66, 8411014151, 'MURCIELAGO Nº12', 'La exasesina en serie Kômori Kuroko vive en una ciudad asediada por la violencia. Con un historial de 715 asesinatos, ha sido contratada por la policía japonesa.', 17, 2, 12.5, 'poridentidad (4).jpg'),
(67, 8418776274, 'CITY HUNTER Nº09', 'El sueño de Ryo Saeba está a punto de hacerse realidad… ¡Una hermosa mujer ha caído en sus brazos y está dispuesta a casarse con él!', 17, 38, 12.5, 'city (1).jpg'),
(68, 8467948370, 'NO GUNS LIFE Nº10', '¿QUÉ ESTÁ TRAMANDO LA AGENCIA DE RECONSTRUCCIÓN?.Jûzô recibe un encargo muy particular del Departamento de Defensa, un organismo fundado tras una remodelación del ejército', 15, 6, 8.5, '9e8e4634f509207185bc2a42fa4fe84bb1cb587a.jpeg'),
(69, 8467948370, 'NO GUNS LIFE Nº10', '¿QUÉ ESTÁ TRAMANDO LA AGENCIA DE RECONSTRUCCIÓN?.Jûzô recibe un encargo muy particular del Departamento de Defensa, un organismo fundado tras una remodelación del ejército', 15, 6, 8.5, '9e8e4634f509207185bc2a42fa4fe84bb1cb587a (1).jpeg'),
(70, 8841918516, 'SUN-KEN ROCK Nº03', 'La historia se centra en Ken, un chico huérfano por culpa de los lazos de su familia con la Yakuza', 16, 21, 9.5, 'porreferencia.jpg'),
(71, 8841101312, 'MURCIELAGO Nº11', 'La exasesina en serie Kômori Kuroko vive en una ciudad asediada por la violencia. Con un historial de 715 asesinatos.', 17, 1, 8.5, 'poridentidad (5).jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicacion_autor`
--

CREATE TABLE `publicacion_autor` (
  `Dni_autor` mediumint(9) NOT NULL,
  `Cod_interno` mediumint(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `publicacion_autor`
--

INSERT INTO `publicacion_autor` (`Dni_autor`, `Cod_interno`) VALUES
(2, 49),
(2, 54),
(3, 50),
(3, 52),
(4, 51),
(5, 53),
(6, 55),
(7, 56),
(8, 57),
(8, 58),
(8, 62),
(9, 59),
(10, 60),
(11, 61),
(12, 63),
(12, 64),
(13, 65),
(14, 66),
(14, 71),
(15, 67),
(16, 68),
(16, 69),
(17, 70),
(18, 19),
(19, 21),
(20, 23),
(21, 25),
(22, 26),
(23, 27),
(24, 28),
(25, 29),
(26, 30),
(27, 31),
(28, 32),
(28, 34),
(29, 33),
(30, 35),
(31, 36),
(32, 37),
(33, 38),
(34, 39),
(35, 40),
(36, 41),
(36, 48),
(39, 42),
(39, 43),
(39, 44),
(39, 45),
(39, 47),
(40, 46);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicacion_tipo`
--

CREATE TABLE `publicacion_tipo` (
  `Cod_tipo` mediumint(9) NOT NULL,
  `Cod_interno` mediumint(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo`
--

CREATE TABLE `tipo` (
  `Cod_tipo` mediumint(9) NOT NULL,
  `Descripcion_Tipo` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_usuario`
--

CREATE TABLE `tipo_usuario` (
  `Cod_tipo_usuario` int(1) NOT NULL,
  `Descripcion_Usuario` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo_usuario`
--

INSERT INTO `tipo_usuario` (`Cod_tipo_usuario`, `Descripcion_Usuario`) VALUES
(1, 'admin'),
(2, 'cliente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `Cod_usuario` mediumint(9) NOT NULL,
  `Nombre_usuario` varchar(11) NOT NULL,
  `Apellido` varchar(20) NOT NULL,
  `Telefono` int(9) NOT NULL,
  `Alias` varchar(10) NOT NULL,
  `Pass` varchar(32) NOT NULL,
  `Direccion` varchar(30) NOT NULL,
  `Cod_tipo_usuario` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`Cod_usuario`, `Nombre_usuario`, `Apellido`, `Telefono`, `Alias`, `Pass`, `Direccion`, `Cod_tipo_usuario`) VALUES
(0, 'joseba', 'm', 12, 'joseba', '1234', 'dsfg', 1),
(1, 'david', 'm', 12, 'david', '1234', 'dsfg', 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `autor`
--
ALTER TABLE `autor`
  ADD PRIMARY KEY (`Dni_autor`);

--
-- Indices de la tabla `editorial`
--
ALTER TABLE `editorial`
  ADD PRIMARY KEY (`Cod_editorial`);

--
-- Indices de la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`Cod_pedido`),
  ADD UNIQUE KEY `Cod_usuario` (`Cod_usuario`);

--
-- Indices de la tabla `pedido_publicacion`
--
ALTER TABLE `pedido_publicacion`
  ADD PRIMARY KEY (`Cod_pedido`,`Cod_interno`),
  ADD KEY `Cod_interno` (`Cod_interno`);

--
-- Indices de la tabla `publicacion`
--
ALTER TABLE `publicacion`
  ADD PRIMARY KEY (`Cod_interno`),
  ADD KEY `Cod_editorial` (`Cod_editorial`);

--
-- Indices de la tabla `publicacion_autor`
--
ALTER TABLE `publicacion_autor`
  ADD PRIMARY KEY (`Dni_autor`,`Cod_interno`),
  ADD KEY `Cod_interno` (`Cod_interno`);

--
-- Indices de la tabla `publicacion_tipo`
--
ALTER TABLE `publicacion_tipo`
  ADD PRIMARY KEY (`Cod_tipo`,`Cod_interno`),
  ADD KEY `Cod_interno` (`Cod_interno`);

--
-- Indices de la tabla `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`Cod_tipo`);

--
-- Indices de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  ADD PRIMARY KEY (`Cod_tipo_usuario`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`Cod_usuario`),
  ADD UNIQUE KEY `Cod_tipo_usuario` (`Cod_tipo_usuario`),
  ADD UNIQUE KEY `Alias` (`Alias`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `editorial`
--
ALTER TABLE `editorial`
  MODIFY `Cod_editorial` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `publicacion`
--
ALTER TABLE `publicacion`
  MODIFY `Cod_interno` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  MODIFY `Cod_tipo_usuario` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `pedido_ibfk_1` FOREIGN KEY (`Cod_usuario`) REFERENCES `usuario` (`Cod_usuario`);

--
-- Filtros para la tabla `pedido_publicacion`
--
ALTER TABLE `pedido_publicacion`
  ADD CONSTRAINT `pedido_publicacion_ibfk_1` FOREIGN KEY (`Cod_pedido`) REFERENCES `pedido` (`Cod_pedido`),
  ADD CONSTRAINT `pedido_publicacion_ibfk_2` FOREIGN KEY (`Cod_interno`) REFERENCES `publicacion` (`Cod_interno`);

--
-- Filtros para la tabla `publicacion`
--
ALTER TABLE `publicacion`
  ADD CONSTRAINT `publicacion_ibfk_1` FOREIGN KEY (`Cod_editorial`) REFERENCES `editorial` (`Cod_editorial`);

--
-- Filtros para la tabla `publicacion_autor`
--
ALTER TABLE `publicacion_autor`
  ADD CONSTRAINT `publicacion_autor_ibfk_1` FOREIGN KEY (`Dni_autor`) REFERENCES `autor` (`Dni_autor`),
  ADD CONSTRAINT `publicacion_autor_ibfk_2` FOREIGN KEY (`Cod_interno`) REFERENCES `publicacion` (`Cod_interno`);

--
-- Filtros para la tabla `publicacion_tipo`
--
ALTER TABLE `publicacion_tipo`
  ADD CONSTRAINT `publicacion_tipo_ibfk_1` FOREIGN KEY (`Cod_tipo`) REFERENCES `tipo` (`Cod_tipo`),
  ADD CONSTRAINT `publicacion_tipo_ibfk_2` FOREIGN KEY (`Cod_interno`) REFERENCES `publicacion` (`Cod_interno`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`Cod_tipo_usuario`) REFERENCES `tipo_usuario` (`Cod_tipo_usuario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
