
<?php
session_start();
require_once '../conexion.php';
$objec_conexion =new conectar();
$db=$objec_conexion->connect();

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="../img/book1 (1).ico" type="image/x-icon">
    <link rel="stylesheet" href="../style/bootstrap.min.css">

    <!--CSS https://www.todostuslibros.com/-->
        <!-- <link href="style/app.css?id=323a540283059dfee5ff" rel="stylesheet">
        <link href="/front.css?id=146a1931b2fd1092535d" rel="stylesheet"> -->
        <link href="https://www.todostuslibros.com/css/fontawesome-5.12.0/css/all.min.css" rel="stylesheet">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" >
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" ></script>

    <script src="../Alert/sweetalert-dev.js"></script>
    <link rel="stylesheet" href="../Alert/sweetalert.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">

    <!-- ESTILO CURSOS DE PROGRAMACION -->
    <link rel="stylesheet" href="../css/style_cp.css">

    <title>Libros</title>
</head>

<body>
<?php
        include_once ("../header.php");
        
       
    ?>
<!------------DIV DEL BUSCADOR-------------------------------------------------------------------->

    <br>
    <h2 style="text-align:center">BÚSQUEDA DE LIBROS</h2>
    <section class="jumbotron text-center">
    <div class="container border-custom">
        <div class="search-container text-center">
            <form method="POST" action="http://localhost/git/sistema-facturacion/php/buscadorjoin1.php" id="main-search-form">
                <div class="input-group input-group-lg">
                    <input type="text" name="buscar" value="" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" placeholder="Título, Autor, ISBN...">
                    <div class="input-group-prepend">
                        <button type="submit" class="input-group-text" id="inputGroup-sizing-lg"><i class="fas fa-search"></i><span>Buscar</span></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </section>
    <br>


<!------------------------------------------------------------------------------------------------------------>




    </div>
   
<!------------FIN DE DIV DE LA TIENDA-------------------------------------------------------------------->


<!------------FIN DE DIV DEL BUSCADOR-------------------------------------------------------------------->


        <!-- vista A -->
<div class="center mt-5">
    <div class="card pt-3" >
            <p style="font-weight: bold; color: #0F6BB7; font-size: 22px;">Carrito de compra</p>
        <div class="container-fluid p-2" style="background-color: ghostwhite;">

            <?php 
                $sql1="select * from publicacion";
                $resultado=$db->query($sql1);
              ?>

            <!-- <h5 class="card-tittle">Resultados (<?php //echo $numero; ?>)</h5> -->
            <!-- <div class="container_card"> -->
           
              <?php while ($fila=$resultado->fetch(PDO::FETCH_ASSOC)){
            
                    ?>

                    <form id="formulario" name="formulario" method="post" action="cart.php">
                        <div class="blog-post ">
                            <img src="../img/<?php echo $fila["img"]; ?>" >
                            <a class="category">
                                <?php echo $fila["Precio_publicacion"]; ?>€
                            </a>
                                <div class="text-content">
                                    <input name="ref" type="hidden" id="ref" value="<?php echo $fila["Cod_publico"];?>" />                           
                                    <input name="precio" type="hidden" id="precio" value="<?php echo $fila["Precio_publicacion"]; ?>" />
                                    <input name="titulo" type="hidden" id="titulo" value="<?php echo $fila["Titulo"]; ?>" />
                                    <input name="cantidad" type="hidden" id="cantidad" value="1" class="pl-2" />
                                        <div class="card-body">
                                                <h5 class="card-title"><?php echo $fila["Titulo"]; ?></h5>
                                                <p><?php echo $fila["Descripcion_publicacion"]; ?></p>
                                                <button class="btn btn-primary" type="submit" ><i class="fas fa-shopping-cart"></i> Añadir al carrito</button>
                                        </div>
                                </div>
                        </div>
                    </form>
                    <?php } ?>
             
            </div>
        </div>
    </div>
                
</div>
<!-- END vista A -->











<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" ></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" ></script>

</body>
</html>
    <!--div del footer------------------------------------>
    <?php
    include_once "../footer.php";

     include "modal_cart.php";
    ?>


    <!--script de JS asociado al Bootstrap-------------->
    <script src="../js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" ></script>
</body>

</html>