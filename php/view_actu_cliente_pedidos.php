<?php

session_start();
    if (!isset($_SESSION['usuario'])){
      header("location:index.html");
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../style/bootstrap.min.css">
    <link rel="shortcut icon" href="../img/book1 (1).ico" type="image/x-icon">




    <link rel="shortcut icon" href="../img/book1 (1).ico" type="image/x-icon">

     <!--CSS https://www.todostuslibros.com/-->
        <!-- <link href="style/app.css?id=323a540283059dfee5ff" rel="stylesheet">
        <link href="/front.css?id=146a1931b2fd1092535d" rel="stylesheet"> -->
        <link href="https://www.todostuslibros.com/css/fontawesome-5.12.0/css/all.min.css" rel="stylesheet">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" >
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" ></script>

    <script src="../Alert/sweetalert-dev.js"></script>
    <link rel="stylesheet" href="../Alert/sweetalert.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">

    <!-- ESTILO CURSOS DE PROGRAMACION -->
    <link rel="stylesheet" href="../css/style_cp.css">

    <script> 
    var myModal = document.getElementById('myModal')
    var myInput = document.getElementById('myInput')
    myModal.addEventListener('shown.bs.modal', function () {
    myInput.focus()})
    </script>
    
    <title>Document</title>
</head>


<body style="background:white;">
    <?php
        include_once "header.php"
    ?>
       
      
       
       <?php if (!empty($_GET['m']) and !empty($_GET['action']) ) { ?>

        <form class="row g-3" method="POST" enctype="multipart/form-data">
        
        <H1>Actualizar Información de Usuario</H1>
  <div class="col-md-6">
    <label for="inputEmail4" class="form-label">Nombre</label>
    <input class="form-control" id="inputEmail4"  type="text" name="Nombre_usuario" placeholder="Nombre">
  </div>
  <div class="col-md-6">
    <label for="inputPassword4" class="form-label">Apellido</label>
    <input class="form-control" id="inputPassword4" type="text" name="Apellido" placeholder="Apellido">
  </div>
  <div class="col-12">
    <label for="inputAddress" class="form-label">Teléfono</label>
    <input  class="form-control" id="inputAddress" type="text" name="Telefono" placeholder="Teléfono">
  </div>
  <div class="col-12">
    <label for="inputAddress2" class="form-label">Dirección</label>
    <input type="text" class="form-control" id="inputAddress2" name="Direccion" placeholder="Dirección">
  </div>
  <div class="col-md-6">
    <label for="inputCity" class="form-label">Alias</label>
    <input type="text" class="form-control" id="inputCity" name="Alias" placeholder="Alias">
  </div>
  
  <div class="col-12">
    <input id="buton2c" type="submit" value="Mandar" class="btn btn-primary" onclick="action='?action=actualizar';"/>	
  </div>
</form>

<br>
      
    <?php } else {
        echo '<h4>Hola</h4>'.$_SESSION['usuario']  ;
     }  ?>



    <!--FOOTER------------------------------------>
    <?php
    include_once "footer.php";
    
    ?>
    <!--script de JS asociado al Bootstrap-------------->
    <script src="../js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" ></script>
    
</body>

</html>